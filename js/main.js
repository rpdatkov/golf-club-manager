var dropButton = document.getElementById("dropBtn");
var dropMenu = document.querySelector(".dropmenu");

//Event listener for drop down
$(dropButton).click(function() {    
      $(dropMenu).slideToggle();
})

//getting the images in the 'blocks' div
var images = document.getElementsByTagName('img');
var videos = document.querySelector('.video');
var photo = document.querySelector('.photo');
var rss = document.querySelector('.rss');
var person = document.querySelector('.person');

var videosImg = images[15];
var photosImg = images[16];
var rssImg = images[17];
var personImg = images[18];

$(videos).mouseenter(function (){
    $(videosImg).attr('src','img/videos-hover.png');
});

$(videos).mouseleave(function (){
    $(videosImg).attr('src', 'img/videos.png');
});

$(photo).mouseenter(function (){
    $(photosImg).attr('src','img/photos-hover.png');
});

$(photo).mouseleave(function (){
    $(photosImg).attr('src', 'img/photos.png');
});

$(rss).mouseenter(function (){
    $(rssImg).attr('src','img/rss-hover.png');
});

$(rss).mouseleave(function (){
    $(rssImg).attr('src', 'img/rss.png');
});

$(person).mouseenter(function (){
    $(personImg).attr('src','img/person-hover.png');
});

$(person).mouseleave(function (){
    $(personImg).attr('src', 'img/person.png');
});